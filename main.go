package main

import (
	"bytes"
	"fmt"
	"log"
	"strings"

	"encoding/json"
	"io/ioutil"
	"net/http"
	"os"
	"os/user"
	"path"
	"strconv"

	"time"

	"github.com/jasonlvhit/gocron"
)

const (
	fn               = ".hn_check"
	latestStoriesURL = "https://hacker-news.firebaseio.com/v0/newstories.json"
	itemURLf         = "https://hacker-news.firebaseio.com/v0/item/%d.json"
	hnItemURL        = "https://news.ycombinator.com/item?id=%d"
	serviceURL       = "https://script.google.com/macros/s/AKfycbycdYCExUoddORajg5smkyUwAvzWar5t2qC-uPFwYJzTehOigY/exec"
)

var domainMail map[string]string

func getHomeDir() string {
	u, _ := user.Current()
	return u.HomeDir
}

func getHNCheckFilePath() string {
	return path.Join(getHomeDir(), fn)
}

func getLastStoryID() int {
	// open the file for reading the last item no.
	var file *os.File
	var lastStoryID int
	if _, err := os.Stat(fn); os.IsNotExist(err) {
		// file does not exist
		// create a new file
		if file, err = os.Create(getHNCheckFilePath()); err != nil {
			log.Fatalf("error while creating %s file.\n%s", fn, err)
		} else {
			log.Printf("%s file created\n", fn)
			lastStoryID = -1 // since all story ids are positive, setting this to -1 for comparision
		}

	} else {
		// file exists, open it
		file, err = os.OpenFile(getHNCheckFilePath(), os.O_APPEND|os.O_RDWR, 0666)
		if err != nil {
			log.Fatalf("error opening file %s.\n%s", fn, err)
		}
		contents, err := ioutil.ReadAll(file)
		if err != nil {
			err = fmt.Errorf("error occured while reading the contents of file %s.\n%s", fn, err)
			log.Fatalln(err)
		}
		if len(contents) == 0 {
			lastStoryID = -1
		} else {
			lastStoryID, err = strconv.Atoi(string(contents))
			if err != nil {
				log.Fatalln(err)
			}
		}
	}
	defer file.Close()
	return lastStoryID
}

func checkStory(storyID int, domains []string, actions []itemFoundAction) {
	// get the story url data
	itemResp, err := http.Get(fmt.Sprintf(itemURLf, storyID))
	if err != nil {
		log.Fatalln("error making the request", err)
	}
	defer itemResp.Body.Close()
	var item map[string]interface{}
	itemRespBody, err := ioutil.ReadAll(itemResp.Body)
	logErr(err)
	err = json.NewDecoder(strings.NewReader(string(itemRespBody))).Decode(&item)
	logErr(err)
	// log.Println("item:", item)
	for _, domain := range domains {
		if _, found := item["url"]; found {
			itemURL := item["url"].(string)
			if strings.Contains(itemURL, domain) {
				for _, action := range actions {
					action(domain, storyID, item)
				}
			}
		}
	}
}

func checkHN(domains []string) {
	log.Printf("starting run at %v\n", time.Now())
	lastStoryID := getLastStoryID()
	log.Println("Last story ID:", lastStoryID)

	// get the latest stories from HN
	log.Println("fetching the ids of the latest stories...")
	resp, err := http.Get(latestStoriesURL)
	if err != nil {
		log.Fatalln("error making the request", err)
	}
	defer resp.Body.Close()
	log.Println("fetching new stories ids done")

	b, _ := ioutil.ReadAll(resp.Body)
	// fmt.Println(string(b))

	// iterate over the new stories till the first story that was checked on the last run
	log.Println("fetching details for each of the new stories and checking for domains...")
	var stories []int
	r := strings.NewReader(string(b))
	err = json.NewDecoder(r).Decode(&stories)
	if err != nil {
		log.Fatalln("error parsing the json for stories ids: ", err)
	}
	for _, storyID := range stories {
		if storyID == lastStoryID {
			break
		}
		checkStory(storyID, domains, []itemFoundAction{printIfFound, sendNotificationEmail})
	}

	// write the first story that has been checked in this run to the hn_check file
	err = ioutil.WriteFile(getHNCheckFilePath(), []byte(strconv.Itoa(stories[0])), 0666)
	if err != nil {
		log.Fatalln("error updating hn_check:", err)
	}
	fmt.Printf("completed at %v\n", time.Now())
}

func main() {
	// domains to check
	domains := os.Args[1:]
	if len(domains) == 0 {
		fmt.Println("Enter atleast one domain to be checked")
		os.Exit(1)
	}
	log.Println("checking domains:", strings.Join(domains, ", "))

	domainMail = make(map[string]string)
	for _, domain := range domains {
		domainMail[domain] = "shashanksharma21@gmail.com"
	}
	fmt.Println(domainMail)

	gocron.Every(30).Minutes().Do(checkHN, domains)
	<-gocron.Start()
}

func logErr(err error) {
	if err != nil {
		log.Println(err)
	}
}

type itemFoundAction func(domain string, storyID int, item map[string]interface{})

func printIfFound(domain string, storyID int, item map[string]interface{}) {
	fmt.Println("found", domain, "in story:", fmt.Sprintf(hnItemURL, storyID))
}

func sendNotificationEmail(domain string, storyID int, item map[string]interface{}) {
	token1 := os.Getenv("GAS_HN_NOTIFIER_TOKEN_1")
	token2 := os.Getenv("GAS_HN_NOTIFIER_TOKEN_2")

	payloadJSON := make(map[string]interface{})
	payloadJSON["token1"] = token1
	payloadJSON["token2"] = token2
	payloadJSON["func"] = "notifyViaEmail"
	payloadJSON["emailTo"] = domainMail[domain]
	payloadJSON["domain"] = domain
	payloadJSON["storyURL"] = fmt.Sprintf("https://news.ycombinator.com/item?id=%d", storyID)

	fmt.Println(payloadJSON)
	jsonValue, err := json.Marshal(payloadJSON)
	logErr(err)
	resp, err := http.Post(serviceURL, "application/json", bytes.NewBuffer(jsonValue))
	logErr(err)
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	logErr(err)
	log.Println(string(body))
}
